# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.4

- patch: Documentation updates

## 0.1.3

- patch: Fixed the default value for MODE parameter

## 0.1.2

- patch: Update the yaml definition

## 0.1.1

- patch: Updated contributing guidelines

## 0.1.0

- minor: Initial release

